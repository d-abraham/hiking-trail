/**
 * Trail has three gates, when a stack of hiker is full (10-hikers) will randomly
 * assigned to a gate.
 * NOTE: Any group must be a size of 10.
 *@author d-abraham
 */

public class Trail {
	private ArrayQueue<HikersGroup> gate1, gate2, gate3;
	private int gate1Total, gate2Total, gate3Total;
	
	/**
	 * Class constructor with the default arrayQueue size of 10 
	 */
	public Trail() {
		gate1 = new ArrayQueue();
		gate2 = new ArrayQueue();
		gate3 = new ArrayQueue();
		gate1Total = 0;
		gate2Total = 0;
		gate3Total = 0;
	}
	
	/**
	 * Randomly adds a group of hikers to one of the three gates.
	 * Group size must be 10 or IllegalArgumentException will be thrown.
	 * @param group to be added
	 */
	public void addHikersGroup(HikersGroup group) {
		if(group.getSize() <10) {
			throw new IllegalArgumentException("Stack size must be 10.");
		}
		else {
			int gateNum = (int) ((Math.random()*3)+1);
			switch (gateNum) {
			case 1:
				gate1.offer(group);
				gate1Total++;
				break;
			case 2:
				gate2.offer(group);
				gate2Total++;
				break;
			case 3:
				gate3.offer(group);
				gate3Total++;
				break;
			}
		}
	}
	
	/**
	 * Returns the specified gate
	 * @param gateNum 
	 * @return gate
	 */
	public ArrayQueue<HikersGroup> getGate(int gateNum){
		switch (gateNum) {
		case 1:
			return gate1;
		case 2:
			return gate2;
		case 3:
			return gate3;
		default:
			return null;
		}
	}
	
	/**
	 * Releases one group from the specified gate, group will be removed from queue.
	 * @param gate number
	 * @return hikersGroup
	 */
	public HikersGroup releaseHikers(int gate) {
		if(gate == 1) {
			if(!gate1.isEmpty()) {
				return gate1.poll();
			}
			else {return null;}
		}
		else if (gate == 2) {
			if(!gate2.isEmpty()) {
				return gate2.poll();
			}
			else {return null;}
		}
		else {
			if(!gate3.isEmpty()) {
				return gate3.poll();
			}
			else {return null;}
		}
	}
	
	public String toString() {
		String s = "Gate 1 has " + gate1.size() + " hiker(s) group ready and waiting."
	            +"\n" + "Gate 2 has "
				+ gate2.size() + " hiker(s) group ready and waiting." 
	            + "\n" + "Gate 3 has " + gate3.size()
				+ " hiker(s) group ready and waiting.";

		return s;
	}

}
