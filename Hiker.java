/**
 *Hiker Class holds the hiker id.  
 *
 * @author d-abraham
 */
public class Hiker{

  private int id;

  /**
   * Construct a hiker with no id value
   */
  public Hiker(){
    this.id =0;
  }
  
  /**
	 * Construct a Hiker with the given id
	 * @param Hiker's id number
	 */
  public Hiker(int id){
    this.id = id;
  }

  /**
   * GetID returns the hiker's id number.
   * @return id number
   */
  public int getID(){
    return id;
  }

  /**
	 * toString return the hiker id
	 * @return Hiker's id number as a string
	 */
  public String tosString(){
    return "Hiker ID: "+id;
  }
}
