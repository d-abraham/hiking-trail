import java.util.Stack;
/**
 * Hikers must be in group of 10 to get to the gates. HikersGroup represent a group 
 * of 10 hikers, which will later randomly assigned to one of the three gates.
 * Each group will have a group ID number, max size of 10 and size variable to keep 
 * track of how many hikers in the group<stack>. 
 * Group ID is just to make it easier to track any hiker in case of emergency.
 *
 * @author d-abraham
 *
 */
public class HikersGroup {
	private Stack<Hiker> group;
	  private final int maxSize = 10;
	  private int size,groupID;

	  /**
		 * Class constructor.
		 */
	  public HikersGroup(){
	    group = new Stack<Hiker>();
	    size = 0;
	    groupID=0;
	  }

	  /**
		 * Class constructor specifying group id.
		 * @param id number
		 */
	  public HikersGroup(int id){
	    group = new Stack<Hiker>();
	    size = 0;
	    groupID = id;
	  }

	  /**
	   * Gets group id number.
	   * @return group id
	   */
	  public int getGroupID(){
	    return groupID;
	  }
	  
	  /**
	   * Gets group size
	   * @return returns size
	   */
	  public int getSize() {
		  return size;
		}

	  /**
	   * Gets the top hiker in the stack using peek(), without removing.
	   * @return Returns hiker 
	   */
	  public Hiker getHiker() {
			return group.peek();
		}

	  /**
	   * Gets the top hiker in the stack using pop(), hiker will be removed from stack.
	   * If stack is empty returns null.
	   * @return Returns hiker 
	   */
	  public Hiker poplHiker() {
		  if(group.empty()) {
			  return null;
		  }
		  else {
			  size--;
				return group.pop();
				}
		  }
	  
	  /**
		 * First, addHiker checks if the stack is full, if not then adds the hiker 
		 * to the stack. If stack is full will throw an Exception.
		 * @param Hiker's id number
		 */
	  public void addHiker(int hikerID) {
			if(size >= maxSize) { // stack max size is 10
				throw new IndexOutOfBoundsException("Stack is full");
			}
			else {
				group.add(new Hiker(hikerID));
				size++;

				}
			}

	  /**
		 * Returns information about the stack.
		 * @return Hikers group id and number of hiker(s).
		 */
	  public String toString() {
			return "Hikers group ID: "+groupID+" number of hiker(s): "+ size;
		}

}
