import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
/**
 * GUI class is responsible for creating all of the GUIs parts and the logic behind
 * each button. 
 * @author d-abraham
 * 
 * Other comments on stack and queue implementation:
 * Why Stack from Java API:
 * 1- Don't reinvent the wheel.
 * 2- The only thing that was missing is the stack size, which was easily fixed by
 * adding size variable in HikersGroup.
 * 
 * Why ArrayQueue using circular array:
 * In real life most businesses would be able to calculate the average of how many 
 * customers they will get, but there are always a chance to get more because of
 * holidays or weekends. Circular array will cover that possibility. 
 *
 */
public class GUI implements ActionListener {
	
	JTextArea text;
	JScrollPane panel ;
	Trail trail;
	
	/**
	 * Class constructor with the default arrayQueue size of 10 
	 */
	public GUI(){
		JFrame frame = new JFrame("Mountain Trail");
		frame.setLayout(new FlowLayout());
		frame.setSize(750,500);
		text = new JTextArea(30,30);
		panel = new JScrollPane(text);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JButton start = new JButton("Start");
		JButton open = new JButton("Open Trail");
		JButton check = new JButton("Check Trail");
		start.addActionListener(this);
		open.addActionListener(this);
		check.addActionListener(this);
		frame.add(panel);
		frame.add(start);
		frame.add(open);
		frame.add(check);
		frame.setVisible(true);
	}

	/**
	 * Start button:
	 * When the start button is clicked it will generate hikers, adds 10 hikers
	 * to HikersGroup, then adds HikersGroup to the Gate<queue>. All of the hikers
	 * and groups information will be printed in the text area.
	 * Also, it will delete any data that was previously written.
	 * 
	 * Open Trail button:
	 * Release/remove HikersGroup from the queue, starts from gate 1-3. Will attempt 
	 * to release 20 hiker at a time, otherwise will release 10. All released/removed
	 * hikers and HikersGroup information will be written in openreport.txt file.
	 * Note: user will have to keep clicking Open Trail button till all gates(queue)
	 * are empty.
	 * 
	 * Check Trail button:
	 * Copy released hikers, their groups and which gate they used to 
	 * signed-out-hikers.txt, then it will print how many hikers went through each
	 * gate.
	 * 
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		/**
		 *   
		 */
		if(e.getActionCommand().matches("Start")) {
			trail = new Trail();
			text.append("Starting trail: \n");
			for (int i = 0; i < 10; i++) {
				HikersGroup group = new HikersGroup(i);
				text.append("Hikers in group-ID: "+group.getGroupID()+" :\n");
				for (int j = 0; j < 10; j++) {
					group.addHiker(j);
					text.append(group.getHiker().tosString()+"\n");					
				}
				trail.addHikersGroup(group);			
			}
			text.append("Starting report: \n"+trail.toString()+"\n");
			try { // Only to delete old data in text files
				FileWriter openreport = new FileWriter("openreport.txt");
				FileWriter writer = new FileWriter("signed-out-hikers.txt");
			}catch (IOException exc) {
				System.out.println(exc.getMessage());
			}
		}
		else if (e.getActionCommand().matches("Open Trail")) {
			HikersGroup temp;
			try {
				FileWriter openreport = new FileWriter("openreport.txt", true);
				for (int i = 0; i < 2; i++) {
					temp=trail.releaseHikers(1);
					if(temp!=null) {
						openreport.write("Gate one:\n");
						for(int h=0; h<10; h++) {
							openreport.write(temp.poplHiker().tosString()+
									" member of group ID#: "+temp.getGroupID()+
									" gate: 1st.\n");
						}
					}
				}
				for (int i = 0; i < 2; i++) {
					temp=trail.releaseHikers(2);
					if(temp!=null) {
						openreport.write("Gate two:\n");
						for(int h=0; h<10; h++) {
							openreport.write(temp.poplHiker().tosString()+
									" member of group ID#: "+temp.getGroupID()+
									" gate: 2nd.\n");
						}
					}
				}
				for (int i = 0; i < 2; i++) {
					temp=trail.releaseHikers(3);
					if(temp!=null) {
						openreport.write("Gate three:\n");
						for(int h=0; h<10; h++) {
							openreport.write(temp.poplHiker().tosString()+
									" member of group ID#: "+temp.getGroupID()+
									" gate: 3rd.\n");
						}
					}
				}
				openreport.close();
			}
			catch (IOException exc) {
				System.out.println(exc.getMessage());
			}
			text.append("Remaing after opening: \n"+trail.toString()+"\n");
		}
		else {
			String line = "";
			BufferedReader reader;
			FileWriter writer;
			int gate1=0,gate2=0,gate3 =0;
			try {
				reader = new BufferedReader(new FileReader("openreport.txt"));
				writer = new FileWriter("signed-out-hikers.txt",true);
				while((line = reader.readLine()) != null){
					writer.write(line+"\n");
					if(line.contains("1st")) {
						gate1++;
					}
					else if(line.contains("2nd")) {
						gate2++;
					}
					else if(line.contains("3rd")){
						gate3++;
					}
				}
				reader.close();
				writer.close();
				text.append("Closing total for each gate:\n"+
				"1st gate total: "+gate1+" hikers."+
						"\n2nd gate total: "+gate2+" hikers."+
						"\n3rd gate total: "+gate3+" hikers.");
			} catch (IOException exc) {
				System.out.println(exc.getMessage());
			}
		}
	}
	
}
